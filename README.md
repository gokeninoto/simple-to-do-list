## To-do list app

###
Frontend part made with React. You can find it in [*frontend_master*](https://gitlab.com/gokeninoto/simple-to-do-list/-/tree/frontend_master) branch.
###
Backend part made with Express as server-side framework and MongoDB as db. You can find it in [*backend_master*](https://gitlab.com/gokeninoto/simple-to-do-list/-/tree/backend_master) branch.
